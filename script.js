/*Дан массив с числами. Найдите сумму
последних N элементов до первого нуля с
конца. Пример: [1, 2, 3, 0, 4, 5, 6] -
суммируем последние 3 элемента, так как
дальше стоит элемент с числом 0.*/
function getSum(arr) {
    let reversedArr = arr.reverse();
    let result = 0;
    for (let i = 0; i < reversedArr.length; i++) {
        if (reversedArr[i] !== 0) {
            result += reversedArr[i];
        } else {
            break;
        }
    }
    return result;
}

/*Функция ggg принимает 2 параметра:
    анонимную функцию, которая возвращает
3 и анонимную функцию, которая
возвращает 4. Верните результатом
функции ggg сумму 3 и 4.*/
const a = () => {
    return 3
};
const b = () => {
    return 4
};

function ggg(oneFunc, twoFunc) {
    return a() + b();
}

ggg(a,b)


/*Напишите функцию под названием divide,
    которая принимает два
параметра:Числитель и знаменатель.
    Функция возвращает результат
числитель/знаменатель. Если знаменатель
равен 0, или оба параметра равны
строке/undefined. Выбросить исключение.*/
function divide(numerator, denominator) {
    if (denominator === 0) {
        return undefined;
    } else if (typeof (numerator) === 'string' && typeof (denominator) === 'string') {
        return undefined;
    } else {
        return numerator / denominator;
    }
}

/*Для заданной строки перепишите с в
обратном порядке слова нечетной длины.
    Слова четной длины – не изменяются.
    Например

reverseOdd("Bananas") ➞ "sananaB"

reverseOdd("One two three four") ➞ "enO owt eerht four"*/
function reverseOdd(str) {
    let splited = str.split(' ');
    let result = [];
    for (let i = 0; i < splited.length; i++) {
        if (splited[i].length % 2 === 1) {
            result.push(splited[i].split('').reverse().join(''));
        } else {
            result.push(splited[i]);
        }
    }
    return result.join(' ');
}


/*Реализуйте класс Student (Студент), который будет
наследовать от класса User. Этот класс должен иметь
следующие свойства: name (имя, наследуется от User),
surname (фамилия, наследуется от User), year (год
поступления в вуз). Класс должен иметь метод getFullName()
(наследуется от User), с помощью которого можно вывести
одновременно имя и фамилию студента. Также класс должен
иметь метод getCourse(), который будет выводить текущий
курс студента (от 1 до 5). Курс вычисляется так: нужно от
текущего года отнять год поступления в вуз. Текущий год
получите самостоятельно.*/
class User {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }

    getFullName() {
        alert(this.name + ' ' + this.surname);
    }
}

class Student extends User {
    constructor(name, surname, year) {
        super(name, surname);
        this.year = year;
    }

    getCourse() {
        let currentTime = new Date();
        let fullYear = currentTime.getFullYear();
        let course = fullYear - this.year;
        if (course > 5) {
            alert(new Error('Student already graduated'));
        } else {
            alert(`Course of student : ${course}`);
        }

    }
}

let student = new Student('Yevhenii', 'Vlasenko', 2020);

/*Сделайте функцию, которая считает и
выводит количество своих
вызовов(замыкания)*/
function numPlus() {
    let count = 0;
    return function () {
        return count++;
    };
}

let counter = numPlus();

/*Сделайте функцию, каждый вызов
который будет генерировать
случайные числа от 1 до 100, но так,
    чтобы они не повторялись, пока не
будут перебраны все числа из этого
промежутка. Решите задачу через
замыкания - в замыкании должен
храниться массив чисел, которые уже
были сгенерированы функцией..*/

/*!!! Я бы лучше сделал как во втором варианте просто циклом =)*/

function random() {
    let arr = [];
    while (arr.length < 100) {
        let r = Math.floor(Math.random() * 100) + 1;
        if (arr.indexOf(r) === -1) {
            arr.push(r);
        }
    }
    return arr;
}

/*let random = function() {
    let arr = [];
    return function() {
        let num = Math.floor(Math.random() * 100 + 1);
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === num) {return}
        }
        arr.push(num);
    }
}*/


/*Даны дивы. По первому нажатию на
каждый див он красится красн
фоном, по второму красится обратно
и так далее каждый клик происходит
чередование фона. Сделайте так,
чтобы было две функции: одна красит
в красный цвет, другая в зеленый и
они сменяли друг друга через
removeEventListener*/

let div = document.querySelectorAll('.div');

function green() {
    for (let i = 0; i < div.length; i++) {
        div[i].addEventListener('click', () => {
            div[i].style.backgroundColor = 'green';
            div[i].removeEventListener('click', green());
            div[i].addEventListener('click', red());
        })
    }
}

function red() {
    for (let i = 0; i < div.length; i++) {
        div[i].addEventListener('click', () => {
            div[i].style.backgroundColor = 'red';
            div[i].removeEventListener('click', red());
            div[i].addEventListener('click', green());
        })
    }
}

red();

/*Сделайте цепочку из трех промисов.
    Пусть первый промис возвращает
число. Сделайте так, чтобы каждый
последующий промис через 3 секунды
возводил в квадрат результат
предыдущего промиса. После
окончания манипуляций выведите
число алертом на экран.*/

let newPromise = new Promise(function (resolve, reject) {
    return resolve(2);
});
newPromise.then(
    function (result) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * result);
            }, 3000);
        });
    })
    .then(
        function (result) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve(result * result);
                }, 3000);
            });
        })
    .then(
        function (result) {
            alert(result);
        })
    .catch(
        function (e) {
            console.log(e);
        }
    )

/*Модифицируйте:
    Модифицируйте задачу так, чтобы
один из промисов в цепочке
выполнился с ошибкой. В конце
цепочки расположите метод catch,
который поймает эту ошибку.*/

let modPromise = new Promise(function (resolve, reject) {
    return resolve(3);
});
modPromise.then(
    function (result) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(result * result);
            }, 3000);
        });
    })
    .then(
        function (result) {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                    reject(new Error('some error'));
                }, 0);
            });
        })
    .then(
        function (result) {
            alert(result);
        })
    .catch(
        function (e) {
            alert(e);
        }
    )

/*Создайте инпут, в который
пользователь вводит дату своего
рождения в формате '2014-12-31' (с
конкретным годом). По потери
фокуса выведите под инпутом сколько
дней осталось до его дня рождения.
    Воспользуйтесь методом Date.parse.*/
let date = document.querySelector('#date-input');
date.addEventListener('blur', function () {
    let bd = new Date(Date.parse(this.value));
    let now = new Date;
    bd.setFullYear(+now.getFullYear())
    if (bd <= now) {
        bd.setFullYear(+bd.getFullYear() + 1);
    }
    let diff = Math.round((+bd - now) / 86400000);
    document.getElementById('output').innerHTML = diff;
})

/*Дан массив с числами. Оставьте в нем
только положительные числа. Затем
извлеките квадратный корень и этих
чисел*/
function func(elem) {
    return elem > 0
}
let arrTemp = arr.filter(func);
let result = arrTemp.map(function (elem) {
    return Math.sqrt(elem);
});

/*Сделайте промис, который через 5
секунд случайным образом
выполнится или с успехом, или с
ошибкой. Примените изученный
метод catch для отлавливания
ошибок.*/

let newPromise2 = new Promise(function (resolve, reject) {
    let waitTime = () => {return Math.random() * (10 - 1) + 1; };
    setTimeout(() => {
        if (waitTime <= 5) {
            resolve('result');
        } else if (waitTime > 5 && waitTime <= 10) {
            reject('error');
        }
    }, waitTime * 1000);
});
newPromise2.then(
    result => {
        alert('function is good');
    }
)
    .catch(
        error => {
            alert(new Error (name))
        }
    )

/*Даны кнопки. Каждая кнопка по
нажатию на нее выводить следующее
число Фибоначчи. Кнопки работают
независимо. Решить через
замыкания.*/

let buttonFib = function () {
    let a = 0;
    let b = 1;
    let temp;
    return function () {
        this.nextElementSibling.innerHTML = a;
        temp = a;
        a = b;
        b = temp + a;
    }
}
let buttons = document.getElementsByTagName('button');
for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', buttonFib());
}

let input = document.getElementsByClassName('num');
let button = document.querySelector('button');
button.addEventListener('click', func);

/*На странице расположено несколько
форм. В них есть инпуты, в инпутах
числа. Дана кнопка. По нажатию на
эту кнопку циклом переберите все
формы на странице, затем циклом
переберите все инпуты в каждой
форме и найдите сумму чисел из всех
этих инпутов.*/
function getInputSum() {
    let sum = 0;
    for (let i = 0; i < input.length; i++) {
        sum += +input[i].value;
    }
    console.log(sum);
}

/*
Разработайте функцию-конструктор,
    которая будет создавать объект
Human(человек) создайте массив
объектов и реализуйте функцию,
    которая будет сортировать элементы
массива по значению свойства Age по
возрастанию или по убыванию.
Разработайте функцию-конструктор,
которая будет создавать объект
Human(человек) добавьте на свое
усмотрение свойства и методы в этот
объект. Подумайте какие методы и
свойства следует сделать уровня
экземпляра, а какие уровня функции-
конструктора.*/
class Human {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    static getSchemeOfTheVenousStructure() {
        return 'For example schema of human vein diagram';
    }

    getName() {
        return this.name
    }
}
let Ivan = new Human('Ivan', 28);
let Pasha = new Human('Pasha', 22);
let Masha = new Human('Masha', 74);
let Daria = new Human('Daria', 25);
let peoples = [Ivan, Pasha, Masha, Daria];

function sortByAge(arr) {
    if (arr instanceof Array) {
        return arr.sort(func);
    } else console.log('Error');

    function func(a, b) {
        return a.age - b.age;
    }
}

/*
Задача 1
Некая сеть фастфудов предлагает несколько видов гамбургеров:
    маленький (50 тугриков, 20 калорий)
большой (100 тугриков, 40 калорий)
Гамбургер может быть с одним из нескольких видов начинок (обязательно):
сыром (+ 10 тугриков, + 20 калорий)
салатом (+ 20 тугриков, + 5 калорий)
картофелем (+ 15 тугриков, + 10 калорий)
Дополнительно, гамбургер можно посыпать приправой (+ 15 тугриков, 0 калорий) и полить майонезом (+ 20 тугриков, + 5 калорий).
Напиши программу, расчиытвающую стоимость и калорийность гамбургера. Используй ООП подход (подсказка: нужен класс Гамбургер, константы, методы для выбора опций и рассчета нужных величин).
Код должен быть защищен от ошибок. Представь, что твоим классом будет пользоваться другой программист. Если он передает неправильный тип гамбургера, например, или неправильный вид добавки, должно выбрасываться исключение (ошибка не должна молча игнорироваться).
Написанный класс должен соответствовать следующему jsDoc описанию (то есть содержать указанные методы, которые принимают и возвращают данные указанного типа и выбрасывают исключения указанного типа.
*/
class Ingredient {
    constructor(id, category, name, price, kkal) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.price = price;
        this.kkal = kkal;
    }
}

class Burger {
    constructor(burgerSize = null, insideIngredient = null) {
        this.burgerSize = burgerSize;
        this.insideIngredient = insideIngredient;
        this.outerIngredients = new Set();
    }

    get isReady() {
        return this.burgerSize != null && this.insideIngredient != null;
    }

    get moneySum() {
        if (!this.isReady) {
            throw new Error("Isn't ready");
        }
        let sum = this.burgerSize.price;
        sum += this.insideIngredient.price;
        for (let item of this.outerIngredients) {
            sum += item.price;
        }
        return sum;
    }

    get kkalSum() {
        if (!this.isReady) {
            throw new Error("Isn't ready");
        }
        let sum = this.burgerSize.kkal;
        sum += this.insideIngredient.kkal;
        for (let item of this.outerIngredients) {
            sum += item.kkal;
        }
        return sum;
    }

    get name() {
        if (!this.isReady) {
            throw new Error("Isn't ready");
        }
        let name = this.burgerSize.name + " (";
        name += this.insideIngredient.name;
        for (let item of this.outerIngredients) {
            name += ", " + item.name;
        }
        return name + ")";
    }
}

class BurgerBuilder {
    get isReady() {
        return this.burger.isReady;
    };

    constructor() {
        this.burger = new Burger();
    }

    withInside(ingredient) {
        if (!ingredient || ingredient.category !== 1) {
            throw new Error("WithInside");
        }

        this.burger.insideIngredient = ingredient;
        return this;
    }

    withSize(ingredient) {
        if (!ingredient || ingredient.category !== 0)
            throw new Error("WithSize");
        this.burger.burgerSize = ingredient;
        return this;
    }

    build() {
        if (!this.isReady)
            throw new Error("Isn't ready");
        return this.burger;
    }
}

class BurgerDecorator {
    constructor(burger) {
        this.burger = burger;
    }

    get burgerSize() {
        return this.burger.burgerSize;
    }

    set burgerSize(value) {
        burger.burgerSize = value;
    }

    get insideIngredient() {
        return this.burger.insideIngredient;
    }

    set insideIngredient(value) {
        this.burger.insideIngredient = value;
    }

    get outerIngredients() {
        return this.burger.outerIngredients;
    }

    get moneySum() {
        return this.burger.moneySum;
    }

    get kkalSum() {
        return this.burger.kkalSum;
    }

    get name() {
        return this.burger.name;
    }
}

class SpeciesBurger extends BurgerDecorator {
    constructor(burger) {
        super(burger);
        this.burger.outerIngredients.add(ingredients[5]);
    }
}

class MaionezBurger extends BurgerDecorator {
    constructor(burger) {
        super(burger);
        this.burger.outerIngredients.add(ingredients[6]);
    }
}

let ingredients = [
    new Ingredient(1, 0, "маленький бургер", 50, 20),
    new Ingredient(2, 0, "большой бургер", 100, 40),
    new Ingredient(3, 1, "сыр", 10, 20),
    new Ingredient(4, 1, "салат", 20, 5),
    new Ingredient(5, 1, "картофель", 15, 10),
    new Ingredient(6, 2, "приправа", 15, 0),
    new Ingredient(7, 2, "майонез", 20, 5),
];

/*Задача 2
Допустим, у вас есть функция primitiveMultiply, которая в 50% случаев
перемножает 2 числа, а в остальных случаях выбрасывает исключение типа
MultiplicatorUnitFailure. Напишите функцию, обёртывающую эту, и просто
вызывающую её до тех пор, пока не будет получен успешный результат.
    Убедитесь, что вы обрабатываете только нужные вам исключения.
    function MultiplicatorUnitFailure() {}
function primitiveMultiply(a, b) {
    if (Math.random() < 0.5)
        return a * b;
    else
        throw new MultiplicatorUnitFailure();
}
function reliableMultiply(a, b) {
// Ваш код
}
console.log(reliableMultiply(8, 8));
function MultiplicatorUnitFailure() {}
function primitiveMultiply(a, b) {
    if (Math.random() < 0.5)
        return a * b;
    else
        throw new MultiplicatorUnitFailure();
}
function reliableMultiply(a, b) {
// Ваш код
}
console.log(reliableMultiply(8, 8));*/
function MultiplicatorUnitFailure() {
}

function primitiveMultiply(a, b) {
    if (Math.random() < 0.5) {
        return a * b;
    } else {
        throw new MultiplicatorUnitFailure();
    }
}

function reliableMultiply(a, b) {
    let result;
    while (!result) {
        try {
            result = primitiveMultiply(a, b);
        } catch (error) {
            if (!error instanceof MultiplicatorUnitFailure) {
                throw error;
            }
        }
    }
    return result;
}

console.log(reliableMultiply(8, 8));





